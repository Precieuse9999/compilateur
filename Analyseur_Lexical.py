#token = ["Type", "Valeur", "Numéro de ligne"] 
# Si le type est un identificateur alors la valeur sera une chaîne
# Si le type est une constante alors la valeur sera un entier
#while est un mot clef, i est un identificateur, un chiffre est une constante  

Lexique = {}

#Opérateurs arithmétiques
Lexique["+"]= ["Addition", None, None] #
Lexique["-"]= ["Soustraction", None, None]#
Lexique["*"]= ["Multiplication", None, None]#
Lexique["/"]= ["Division", None, None]#
Lexique["%"]= ["Modulo", None, None]#
Lexique["^"]= ["Exposant", None, None]#
Lexique["**"]= ["Exposant", None, None]#
Lexique["="]= ["Affectation", None, None]#

#Opérateurs logiques
Lexique["&&"]= ["And", None, None]#
Lexique["||"]= ["Or", None, None]#
Lexique["!"]= ["Not", None, None]#

#Opérateurs de comparaison ope_comp = [">","<",">=","<=","==","!="]
Lexique[">"]= ["Supérieur à", None, None]#
Lexique["<"]= ["Inférieur à ", None, None]#
Lexique[">="]= ["Supérieur ou égale", None, None]#
Lexique["<="]= ["Inférieur ou égale", None, None]#
Lexique["=="]= ["Egal", None, None]#
Lexique["!="]= ["Est différent de", None, None]#

#Mots-clés
Lexique["const"]= ["Constante", None, None]#
Lexique["ident"]= ["Identificateur", None, None]
Lexique["if"]= ["Si conditionnel", None, None]
Lexique["for"]= ["Boucle for", None, None]
Lexique["while"]= ["Boucle while", None, None]
Lexique["else"]= ["Sinon conditionnel", None, None]

#Délimiteurs
Lexique["("]= ["Début de parenthèse", None, None]
Lexique[")"]= ["Fin de parenthèse",None, None]
Lexique[","]= ["Virgule", None, None]
#INDENTATION !! Indent - Dedent... 

Lexique["EOF"]= ["EOF", None, None]#
Lexique["#"]= ["Ligne de commentaire", None, None]
Lexique["ERR"]= ["ERR", None , None]
#tout mettre en majuscule

#Divers
Lexique["_"]= ["Underscore", None, None]




tab_source=[]
i =0
no_ligne = 0

def analyseLex():
    global i
    file = open("IMC_Calcul.txt","r")
    c = file.read() 
    c = c + " "
    file.close()
    token = unitesuivante(c)
    i = i+1
    print(token)
    tab_source.append(token)
    while token[0] != "EOF" and token[0] != "ERR":
        token = unitesuivante(c) #on prend le prochain token
        i = i+1
        tab_source.append(token) #et on le rajoute à tab_source
        print(token)
        
def unitesuivante(c):
    nombre = ""
    mot = ""
    global no_ligne
    global i
    global tab_source
    while i < len(c)-1 and c[i] == " ": #cette boucle sert à ignorer tous les espaces
        i = i+1
    while i < len(c)-1 and c[i] == "\n": #cette boucle sert à incrémenter les retour à la ligne et il les ignores
        i = i+1  
        no_ligne = no_ligne+1
    if i >= len(c)-1:
        return Lexique["EOF"]
    
    # Si c"est un opérateur simple
    if c[i] == "+":
        token = Lexique["+"]
        token[1]=c[i]
        token[2]=no_ligne
        return token
    if c[i] == "-":
        token = Lexique["-"]
        token[1]=c[i]
        token[2]=no_ligne
        return token
    if c[i] == "/":
        token = Lexique["/"]
        token[1]=c[i]
        token[2]=no_ligne
        return token
    if c[i] == "^":
        token = Lexique["^"]
        token[1]=c[i]
        token[2]=no_ligne
        return token
    if c[i] == "%":
        token = Lexique["%"]
        token[1]=c[i]
        token[2]=no_ligne
        return token
    
    # Si c"est un opérateur double composé de deux opérateurs simples
    if c[i] == "*" and c[i+1] != "*" :
        token = Lexique["*"]
        token[1]=c[i]
        token[2]=no_ligne
        return token 
    if c[i] == "*" and c[i+1] == "*" :
        token = Lexique["**"]
        token[1]=c[i]+c[i+1]
        token[2]=no_ligne
        i = i+1 #pour tous les token double j'incrémente i   
        return token 
    
    if c[i] == "=" and c[i+1] != "=" :
        token = Lexique ["="]
        token[1]=c[i]
        token[2]=no_ligne
        return token 
    if c[i] == "=" and c[i+1] == "=" :
        token = Lexique ["=="]
        token[1]=c[i]
        token[2]=no_ligne
        i = i+1 #pour tous les token double jincrémente l"i 
        return token  
    
    if c[i] == "!" and c[i+1] != "=" :
        token = Lexique ["!"]
        token[1]=c[i]
        token[2]=no_ligne
        return token 
    if c[i] == "!" and c[i+1] == "=" :
        token = Lexique ["!="]
        token[1]=c[i]
        token[2]=no_ligne
        i = i+1 #pour tous les token double jincrémente l"i 
        return token 

    if c[i] == ">" and c[i+1] != "=" :
        token = Lexique [">"]
        token[1]=c[i]
        token[2]=no_ligne
        return token 
    if c[i] == "=" and c[i+1] == "=" :
        token = Lexique [">="]
        token[1]=c[i]
        token[2]=no_ligne
        i = i+1 #pour tous les token double jincrémente l"i 
        return token 
    
    if c[i] == "<" and c[i+1] != "=" :
        token = Lexique ["<"]
        token[1]=c[i]
        token[2]=no_ligne
        return token 
    if c[i] == "<" and c[i+1] == "=" :
        token = Lexique ["<="]
        token[1]=c[i]
        token[2]=no_ligne
        i = i+1 #pour tous les token double jincrémente l"i 
        return token    
    
    # Si c'est un opérateur logique double 
    if c[i] == "&" and c[i+1] == "&" : # dans le cas ou il n'y en a cas seul on peut renvoyer un message d'erreur
        token = Lexique ["&&"]
        token[1]=c[i]+c[i+1]
        token[2]=no_ligne
        i = i+1 #pour tous les token double jincrémente l"i 
        return token  
    if c[i] == "|" and c[i+1] == "|" : # dans le cas ou il n'y en a cas seul on peut renvoyer un message d'erreur
        token = Lexique ["||"]
        token[1]=c[i]
        token[2]=no_ligne
        i = i+1 #pour tous les token double jincrémente l"i 
        return token  
    
    # Si c"est un chiffre 
    if c[i].isnumeric():
        while c[i].isnumeric():#si il croise un charactere qui est un nombre c'est que c'est le debut d'une constante numérique
            nombre = nombre + c[i] #on parcours tous les chiffres
            i = i+1
        token = Lexique["const"] #on sort de la boucle qd on croise autre chose qu'un nombre
        token[1]=int(nombre)#on converti nos charactere en nombre(recoit nombre)
        i = i-1 #on décrémente car pour se rendre compte que le charactere d'apres n'était pas un chiffre il a fallu avancé dans l'i, donc on décrémente pour revenir à ce charactere
        token[2]=no_ligne
        return token
    
    # Si c'est un identificateur
    if c[i].isalpha() :
        while c[i].isalpha() or c[i].isnumeric() or c[i]== "_":#si il croise un charactere qui est un nombre c'est que c'est le debut d'un identificateur
            mot = mot + c[i] #on parcours tous les lettres
            i = i+1
        if mot != motclef(c):  
            token = Lexique["ident"] #crée une fonction qui va tester si nombre est un mot clef
            token[1]= mot 
            token[2]=no_ligne
            i = i-1 #on décrémente car pour se rendre compte que le charactere d'apres n'était pas un chiffre/lettre/underscore il a fallu avancé dans l'i, donc on décrémente pour revenir à ce charactere 
            return token
        if mot == motclef(c):  
            token = Lexique[motclef(c)]
            token[1]= motclef(c)
            token[2]=no_ligne
            return token
        
    # Si c'est un délimiteur 
    
    # Si c'est un commentaire
    
    # Si c'est une erreur
    token = Lexique["ERR"]
    token [1]= i #
    token[2] = no_ligne #quand il y a une erreur on sauvegarde le numero de la ligne 
    return token 
    
def motclef(c):
    mot = ""
    global i
    
    while c[i].isalpha() :
        mot = mot + c[i] #on parcours tous les lettres
        i = i+1           
    if mot == "if":
        return "if"
    
    if mot == "for":
        return "for"
    
    if mot == "while":
        return "while"
    
    if c[i] == "else":
        return "else"
        

analyseLex()